package com.spring.cinema.controller;

import com.spring.cinema.model.Ticket;
import com.spring.cinema.service.TicketService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/tickets")
public class TicketController {

    private TicketService ticketService;

    public TicketController(TicketService ticketService)
    {
        this.ticketService = ticketService;
    }

    @GetMapping
    public List<Ticket> getTickets()
    {
        return this.ticketService.getTickets();
    }

    @PostMapping
    public Ticket saveTicket(@RequestBody Ticket ticket)
    {
        return this.ticketService.saveTicket(ticket);
    }

    @GetMapping("/myticket/{commande}")
    public String getTicket(@PathVariable String commande)
    {
        return this.ticketService.getTicketByCommande(commande);
    }
}
