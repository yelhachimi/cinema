package com.spring.cinema.controller;

import com.spring.cinema.model.Film;
import com.spring.cinema.model.Seance;
import com.spring.cinema.service.SeanceService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/seances")
public class SeanceController {

    private SeanceService seanceService;

    public SeanceController(SeanceService seanceService)
    {
        this.seanceService = seanceService;
    }

    @GetMapping
    public List<Seance> getSeances()
    {
        return this.seanceService.getSeances();
    }

    @GetMapping("{id}")
    public Seance getSeanceById(@PathVariable String id)
    {
        return this.seanceService.getSeanceById(id);
    }

    @PostMapping
    public Seance saveSeance(@RequestBody Seance seance)
    {
        return this.seanceService.saveSeance(seance);
    }

    @DeleteMapping("{id}")
    public String deleteSeance(@PathVariable String id)
    {
        return this.seanceService.deleteSeance(id);
    }

    @GetMapping("{id}/film")
    public Film getFilm(@PathVariable String id)
    {
        return this.seanceService.getSeanceById(id).getFilm();
    }

    @PutMapping
    public Seance updateSeance(@RequestBody Seance seance)
    {
        return this.seanceService.updateSeance(seance);
    }
}
