package com.spring.cinema.controller;

import com.spring.cinema.model.Salle;
import com.spring.cinema.model.Seance;
import com.spring.cinema.service.SalleService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/salles")
public class SalleController {

    private SalleService salleService;

    public SalleController(SalleService salleService)
    {
        this.salleService = salleService;
    }

    @GetMapping
    public List<Salle> getSalles()
    {
        return this.salleService.getSalles();
    }

    @PostMapping
    public Salle saveSalle(@RequestBody Salle salle)
    {
        return this.salleService.saveSalle(salle);
    }

    @GetMapping("{id}")
    public Salle getSalleById(@PathVariable String id)
    {
        return this.salleService.getSalleById(id);
    }

    @DeleteMapping("{id}")
    public String deleteSalleById(String id)
    {
        return this.salleService.deleteSalleById(id);
    }

    @GetMapping("{id}/seances")
    public List<Seance> getSeances(@PathVariable String id)
    {
        return this.salleService.getSalleById(id).getSeances();
    }

    @PutMapping
    public Salle updateSalle(@RequestBody Salle salle)
    {
        return this.salleService.updateSalle(salle);
    }

}
