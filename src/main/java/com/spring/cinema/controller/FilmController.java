package com.spring.cinema.controller;

import com.spring.cinema.model.Film;
import com.spring.cinema.service.FilmService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/films")
public class FilmController {

    private FilmService filmService;

    public FilmController(FilmService filmService)
    {
        this.filmService = filmService;
    }

    @GetMapping
    public List<Film> getAllFilms()
    {
        return this.filmService.getAllFilms();
    }

    @GetMapping("{id}")
    public Film getFilmByNom(@PathVariable String id)
    {
        return this.filmService.getFilmById(id);
    }

    @PostMapping
    public Film saveFilm(@RequestBody Film film)
    {
        return this.filmService.saveFilm(film);
    }

    @DeleteMapping("{id}")
    public String deleteFilm(@PathVariable String id)
    {
        return this.filmService.deleteFilmById(id);
    }

    @PutMapping
    public Film updateFilm(@RequestBody Film film)
    {
        return this.filmService.updateFilm(film);
    }
}
