package com.spring.cinema.controller;

import com.spring.cinema.model.Cinema;
import com.spring.cinema.model.Salle;
import com.spring.cinema.service.CinemaService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/cinemas")
public class CinemaController {

    private CinemaService cinemaService;

    public CinemaController(CinemaService cinemaService)
    {
        this.cinemaService = cinemaService;
    }

    @GetMapping
    public List<Cinema> getCinemas()
    {
        return this.cinemaService.getCinemas();
    }

    @GetMapping("{nom}")
    public Cinema getCinema(@PathVariable String nom)
    {
        return this.cinemaService.getCinema(nom);
    }

    @PostMapping
    public Cinema saveCinema(@RequestBody Cinema cinema)
    {
        return this.cinemaService.saveCinema(cinema);
    }

    @DeleteMapping("{nom}")
    public String deleteCinema(@PathVariable String nom)
    {
        return this.cinemaService.deleteCinema(nom);
    }

    @GetMapping("{nom}/salles")
    public List<Salle> getSalles(@PathVariable String nom)
    {
        return this.cinemaService.getCinema(nom).getSalles();
    }

    @PutMapping
    public Cinema updateCinema(@RequestBody Cinema cinema)
    {
        return this.cinemaService.updateCinema(cinema);
    }

}
