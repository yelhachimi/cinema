package com.spring.cinema.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document
public class Ticket {

    @Id
    private String commande;

    @DBRef
    @Field("cinema")
    private Cinema cinema;

    @DBRef
    @Field("film")
    private Film film;

    @DBRef
    @Field("seance")
    private Seance seance;

    @DBRef
    @Field("salle")
    private Salle salle;
}
