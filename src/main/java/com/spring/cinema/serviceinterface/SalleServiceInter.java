package com.spring.cinema.serviceinterface;

import com.spring.cinema.model.Salle;

import java.util.List;

public interface SalleServiceInter {
    List<Salle> getSalles();

    Salle getSalleById(String id);

    Salle saveSalle(Salle salle);

    String deleteSalleById(String id);

    Salle updateSalle(Salle salle);
}
