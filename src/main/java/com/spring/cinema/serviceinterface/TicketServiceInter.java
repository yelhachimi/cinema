package com.spring.cinema.serviceinterface;

import com.spring.cinema.model.Ticket;

import java.util.List;

public interface TicketServiceInter {

    List<Ticket> getTickets();

    Ticket saveTicket(Ticket ticket);

    String getTicketByCommande(String commande);
}
