package com.spring.cinema.serviceinterface;

import com.spring.cinema.model.Film;

import java.util.List;

public interface FilmServiceInter {
    List<Film> getAllFilms();

    Film getFilmById(String idFilm);

    Film saveFilm(Film film);

    String deleteFilmById(String idFilm);

    Film updateFilm(Film film);
}
