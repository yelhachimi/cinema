package com.spring.cinema.serviceinterface;

import com.spring.cinema.model.Cinema;

import java.util.List;

public interface CinemaServiceInter {
    List<Cinema> getCinemas();

    Cinema getCinema(String nom);

    Cinema saveCinema(Cinema cinema);

    String deleteCinema(String nom);

    Cinema updateCinema(Cinema cinema);
}
