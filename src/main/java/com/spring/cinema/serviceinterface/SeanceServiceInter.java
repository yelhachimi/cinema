package com.spring.cinema.serviceinterface;

import com.spring.cinema.model.Seance;

import java.util.List;

public interface SeanceServiceInter {
    List<Seance> getSeances();

    Seance getSeanceById(String id);

    Seance saveSeance(Seance seance);

    String deleteSeance(String id);

    Seance updateSeance(Seance seance);
}
