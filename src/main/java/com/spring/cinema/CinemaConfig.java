package com.spring.cinema;

import com.spring.cinema.repository.*;
import com.spring.cinema.service.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CinemaConfig {

    @Bean
    public FilmService filmService(FilmRepository filmRepository)
    {
        return new FilmService(filmRepository);
    }

    @Bean
    public SeanceService seanceService(SeanceRepository seanceRepository)
    {
        return new SeanceService(seanceRepository);
    }

    @Bean
    public SalleService salleService(SalleRepository salleRepository)
    {
        return new SalleService(salleRepository);
    }

    @Bean
    public CinemaService cinemaService(CinemaRepository cinemaRepository)
    {
        return new CinemaService(cinemaRepository);
    }

    @Bean
    public TicketService ticketService(TicketRepository ticketRepository)
    {
        return new TicketService(ticketRepository);
    }
}
