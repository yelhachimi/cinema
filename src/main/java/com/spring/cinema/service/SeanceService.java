package com.spring.cinema.service;

import com.spring.cinema.model.Seance;
import com.spring.cinema.repository.SeanceRepository;
import com.spring.cinema.serviceinterface.SeanceServiceInter;

import java.util.List;

public class SeanceService implements SeanceServiceInter {

    private SeanceRepository seanceRepository;

    public SeanceService(SeanceRepository seanceRepository)
    {
        this.seanceRepository = seanceRepository;
    }

    @Override
    public List<Seance> getSeances()
    {
        return this.seanceRepository.findAll();
    }

    @Override
    public Seance getSeanceById(String id)
    {
        return this.seanceRepository.findById(id).get();
    }

    @Override
    public Seance saveSeance(Seance seance)
    {
        return this.seanceRepository.save(seance);
    }

    @Override
    public String deleteSeance(String id)
    {
        Seance seance = this.seanceRepository.findById(id).get();
        String message = "la seance du : "+seance.getDate_debut()+" a été supprimé.";
        this.seanceRepository.deleteById(id);
        return message;
    }

    public Seance updateSeance(Seance seance)
    {
        return this.seanceRepository.save(seance);
    }
}
