package com.spring.cinema.service;

import com.spring.cinema.model.Film;
import com.spring.cinema.model.Salle;
import com.spring.cinema.repository.SalleRepository;
import com.spring.cinema.serviceinterface.SalleServiceInter;

import java.util.List;

public class SalleService implements SalleServiceInter {

    private SalleRepository salleRepository;

    public SalleService(SalleRepository salleRepository)
    {
        this.salleRepository = salleRepository;
    }

    @Override
    public List<Salle> getSalles()
    {
        return this.salleRepository.findAll();
    }

    @Override
    public Salle getSalleById(String id)
    {
        return this.salleRepository.findById(id).get();
    }

    @Override
    public Salle saveSalle(Salle salle)
    {
        return this.salleRepository.save(salle);
    }

    @Override
    public String deleteSalleById(String id)
    {
        Salle salle = this.salleRepository.findById(id).get();
        String message = "la salle : "+salle.getNumero()+" n'est plus disponible.";
        this.salleRepository.deleteById(id);
        return message;
    }

    public Salle updateSalle(Salle salle)
    {
        return this.salleRepository.save(salle);
    }
}
