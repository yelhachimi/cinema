package com.spring.cinema.service;

import com.spring.cinema.model.Film;
import com.spring.cinema.repository.FilmRepository;
import com.spring.cinema.serviceinterface.FilmServiceInter;

import java.util.List;

public class FilmService implements FilmServiceInter {

    private FilmRepository filmRepository;

    public FilmService(FilmRepository filmRepository)
    {
        this.filmRepository = filmRepository;
    }

    @Override
    public List<Film> getAllFilms()
    {
        return this.filmRepository.findAll();
    }

    @Override
    public Film getFilmById(String idFilm)
    {
        return this.filmRepository.findById(idFilm).get();
    }

    @Override
    public Film saveFilm(Film film)
    {
        return this.filmRepository.save(film);
    }

    @Override
    public String deleteFilmById(String idFilm)
    {
        Film film = this.filmRepository.findById(idFilm).get();
        String message = "le Film : "+film.getNom()+" a été supprimé.";
        this.filmRepository.deleteById(idFilm);
        return message;
    }

    public Film updateFilm(Film film)
    {
        return this.filmRepository.save(film);
    }
}
