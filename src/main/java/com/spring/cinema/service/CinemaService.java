package com.spring.cinema.service;

import com.spring.cinema.model.Cinema;
import com.spring.cinema.model.Salle;
import com.spring.cinema.repository.CinemaRepository;
import com.spring.cinema.serviceinterface.CinemaServiceInter;

import java.util.List;

public class CinemaService implements CinemaServiceInter {

    private CinemaRepository cinemaRepository;

    public CinemaService(CinemaRepository cinemaRepository)
    {
        this.cinemaRepository = cinemaRepository;
    }

    @Override
    public List<Cinema> getCinemas()
    {
        return this.cinemaRepository.findAll();
    }

    @Override
    public Cinema getCinema(String nom)
    {
        return this.cinemaRepository.findCinemaByNom(nom);
    }

    @Override
    public Cinema saveCinema(Cinema cinema)
    {
        return this.cinemaRepository.save(cinema);
    }

    @Override
    public String deleteCinema(String nom)
    {
        Cinema cinema = this.cinemaRepository.findCinemaByNom(nom);
        String message = "Le cinema : "+cinema.getNom()+" n'existe plus.";
        this.cinemaRepository.deleteCinemaByNom(nom);
        return message;
    }

    public Cinema updateCinema(Cinema cinema)
    {
        return this.cinemaRepository.save(cinema);
    }
}
