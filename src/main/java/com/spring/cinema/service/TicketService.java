package com.spring.cinema.service;

import com.spring.cinema.model.Ticket;
import com.spring.cinema.repository.TicketRepository;
import com.spring.cinema.serviceinterface.TicketServiceInter;

import java.util.List;

public class TicketService implements TicketServiceInter {

    private TicketRepository ticketRepository;

    public TicketService(TicketRepository ticketRepository)
    {
        this.ticketRepository = ticketRepository;
    }

    public List<Ticket> getTickets()
    {
        return this.ticketRepository.findAll();
    }

    public String getTicketByCommande(String commande)
    {
        Ticket ticket = this.ticketRepository.findById(commande).get();
        String comm = "Commande : "+ticket.getCommande();
        String cinema = "Cinema : "+ticket.getCinema().getNom();
        String film = "Film : "+ticket.getFilm().getNom();
        String date_debut = "Date de début : "+ticket.getSeance().getDate_debut();
        String date_fin = "Date de fin : "+ticket.getSeance().getDate_fin();
        String salle = "Salle : "+ticket.getSalle().getNumero();
        String separateur = System.getProperty("line.separator");

        return comm+separateur+cinema+separateur+film+separateur+date_debut+separateur+date_fin+separateur+salle;
    }

    public Ticket saveTicket(Ticket ticket)
    {
        return this.ticketRepository.save(ticket);
    }
}
