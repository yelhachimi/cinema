package com.spring.cinema.repository;

import com.spring.cinema.model.Salle;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SalleRepository extends MongoRepository<Salle, String> {
}
