package com.spring.cinema.repository;

import com.spring.cinema.model.Cinema;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CinemaRepository extends MongoRepository<Cinema, String> {
    public Cinema findCinemaByNom(String nom);
    public void deleteCinemaByNom(String nom);
}
