package com.spring.cinema.repository;

import com.spring.cinema.model.Seance;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface SeanceRepository extends MongoRepository<Seance, String> {
}
